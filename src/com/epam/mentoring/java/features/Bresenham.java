package com.epam.mentoring.java.features;

import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.awt.Point;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class Bresenham {

    private static List<Point> findLine(Point[][] grid, int x0, int y0, int x1, int y1) {
        List<Point> line = new ArrayList<>();

        BiFunction<Integer, Integer, Integer> delta = (Integer endPointCoordinate, Integer startPointCoordinate)
                -> Math.abs(endPointCoordinate - startPointCoordinate);
        int deltaX = delta.apply(x1, x0);
        int deltaY = delta.apply(y1, y0);

        BiFunction<Integer, Integer, Integer> calculateShift = (Integer startPointCoordinate, Integer endPointCoordinate)
                -> startPointCoordinate < endPointCoordinate ? 1 : -1;
        int shiftX = calculateShift.apply(x0, x1);
        int shiftY = calculateShift.apply(y0, y1);

        int error = deltaX - deltaY;
        int deltaError;

        while (true) {
            line.add(grid[x0][y0]);

            if (x0 == x1 && y0 == y1)
                break;

            deltaError = 2 * error;
            if (deltaError > -deltaY) {
                error -= deltaY;
                x0 += shiftX;
            }

            if (deltaError < deltaX) {
                error += deltaX;
                y0 += shiftY;
            }
        }
        return line;
    }

    private static void draw(Point[][] grid, List<Point> line) {
        System.out.println("\nResulted line : \n");

        IntStream.range(0, grid.length).forEach(i -> {
            IntStream.range(0, grid[0].length).forEach(j ->
            {
                if (line.contains(grid[i][j])) {
                    System.out.print("*");
                } else {
                    System.out.print("-");
                }
            });
            System.out.println();
        });
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter dimensions of grid");
        int rows = scan.nextInt();
        int cols = scan.nextInt();

        Point[][] grid = new Point[rows][cols];

        IntStream.range(0, grid.length).forEach(x -> Arrays.setAll(
                grid[x], y -> grid[x][y] = new Point(x, y)));

        System.out.println("\nPlease enter coordinates of point 1 and point 2");
        int x0 = scan.nextInt();
        int y0 = scan.nextInt();
        int x1 = scan.nextInt();
        int y1 = scan.nextInt();

        List<Point> line = findLine(grid, x0, y0, x1, y1);

        draw(grid, line);
    }
}
